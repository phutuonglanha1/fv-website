<?php

define( 'WP_CACHE', true ); // Added by WP Rocket

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */
define('FS_METHOD','direct');

// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', "db_tuan_nhan" );


/** MySQL database username */

define( 'DB_USER', "root" );


/** MySQL database password */

define( 'DB_PASSWORD', "" );


/** MySQL hostname */

define( 'DB_HOST', "localhost" );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );

define('DISABLE_WP_CRON', true);


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         'Y&Ov<:_e%WAOWlJE5$veTJwQeEjCY#5`Jqmw-B3u2>!_,;FMbnk~-G*9%F->AI8D' );

define( 'SECURE_AUTH_KEY',  '%T3?&VYiA&kqq$JG1)wUp{476<=A7}f55M7o~Pi5PR:c/B+!4%W(.frBK 5P_=dJ' );

define( 'LOGGED_IN_KEY',    'oidE +njv07sm y}=BssY&;zI(BJ$Wi9!9m9x6AUM}{>>cZ1=JYI`YCm~$Arnv*;' );

define( 'NONCE_KEY',        'lW3kp3%8L?Tj7*cmQg;Dp)}C3k3]p[mQ+mK1m[F!AU0pqMDRH4V$H&<4F|~YJ@^#' );

define( 'AUTH_SALT',        'i`si| xkx<%zFI|;+vgg$aX}Z~$iSQPU 91s&/,zwsun]xwt8 4|EMca7%uXhaf)' );

define( 'SECURE_AUTH_SALT', 'a%YrX$,|6k|Y[hSU]5)Jv2G b`) amCl4M*ko$f*]n&5:@<~abKY lKP,k{zft<7' );

define( 'LOGGED_IN_SALT',   'b/u[;cXk(0:vewg&0h~Kt>*NbiF%Y)6PZgJf_=b^z +ASC/5{2fllZ.UbF^2I__n' );

define( 'NONCE_SALT',       '59<*^Fh.sZ/5k)<AEP|hdNp2_eC1HOT}*pq`XYU+h>&}tWb*/k}qRzwCm_1K*fkh' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'nbw_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}


/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );

