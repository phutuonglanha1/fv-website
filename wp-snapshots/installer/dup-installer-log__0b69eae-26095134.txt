********************************************************************************
* DUPLICATOR-PRO: Install-Log
* STEP-1 START @ 09:56:57
* VERSION: 1.4.3
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PACKAGE INFO________ CURRENT SERVER                         |ORIGINAL SERVER
PHP VERSION_________: 7.4.24                                |7.3.28
OS__________________: Linux                                 |Linux
CREATED_____________: 2021-10-26 09:51:34
WP VERSION__________: 5.8.1
DUP VERSION_________: 1.4.3
DB__________________: 10.3.31
DB TABLES___________: 16
DB ROWS_____________: 1,800
DB FILE SIZE________: 3.45MB
********************************************************************************
SERVER INFO
PHP_________________: 7.3.28 | SAPI: fpm-fcgi
PHP MEMORY__________: 4294967296 | SUHOSIN: disabled
SERVER______________: Apache
DOC ROOT____________: "/var/www/vhosts/congtytuannhan.com/httpdocs"
DOC ROOT 755________: true
LOG FILE 644________: true
REQUEST URL_________: "https://congtytuannhan.com/dup-installer/main.installer.php"
********************************************************************************
USER INPUTS
ARCHIVE ACTION______: "donothing"
ARCHIVE ENGINE______: "ziparchive"
SET DIR PERMS_______: 1
DIR PERMS VALUE_____: 1363
SET FILE PERMS______: 1
FILE PERMS VALUE____: 1204
SAFE MODE___________: "0"
LOGGING_____________: "1"
CONFIG MODE_________: "NEW"
FILE TIME___________: "current"
********************************************************************************


--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "/var/www/vhosts/congtytuannhan.com/httpdocs/20191018_mauwebsitebatdongsangiaod_[HASH]_20211026095134_archive.zip"
SIZE________________: 58.96MB

PRE-EXTRACT-CHECKS
- PASS: Apache '.htaccess' not found - no backup needed.
- PASS: Microsoft IIS 'web.config' not found - no backup needed.
- PASS: WordFence '.user.ini' not found - no backup needed.
BEFORE EXTRACION ACTIONS
MAINTENANCE MODE ENABLE


START ZIP FILE EXTRACTION STANDARD >>> 

--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "/var/www/vhosts/congtytuannhan.com/httpdocs/20191018_mauwebsitebatdongsangiaod_[HASH]_20211026095134_archive.zip"
SIZE________________: 58.96MBFile timestamp set to Current: 2021-10-26 09:57:04
<<< ZipArchive Unzip Complete: true
--------------------------------------
POST-EXTACT-CHECKS
--------------------------------------
PERMISSION UPDATES:
    -DIRS:  '755'
    -FILES: '644'

STEP-1 COMPLETE @ 09:57:11 - RUNTIME: 14.5161 sec.



********************************************************************************
* DUPLICATOR-LITE INSTALL-LOG
* STEP-2 START @ 09:57:45
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
VIEW MODE___________: "basic"
DB ACTION___________: "empty"
DB HOST_____________: "**OBSCURED**"
DB NAME_____________: "**OBSCURED**"
DB PASS_____________: "**OBSCURED**"
DB PORT_____________: "**OBSCURED**"
NON-BREAKING SPACES_: false
MYSQL MODE__________: "DEFAULT"
MYSQL MODE OPTS_____: ""
CHARSET_____________: "utf8"
COLLATE_____________: "utf8_general_ci"
COLLATE FB__________: false
VIEW CREATION_______: true
STORED PROCEDURE____: true
FUNCTION CREATION___: true
********************************************************************************

--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 10.3.22 -- Build Server: 10.3.31
FILE SIZE:	dup-database__[HASH].sql (2.04MB)
TIMEOUT:	5000
MAXPACK:	67108864
SQLMODE:	STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
NEW SQL FILE:	[/var/www/vhosts/congtytuannhan.com/httpdocs/dup-installer/dup-installer-data__[HASH].sql]
COLLATE FB:	Off

NOTICE: This servers version [10.3.22] is less than the build version [10.3.31].  
If you find issues after testing your site please referr to this FAQ item.
https://snapcreek.com/duplicator/docs/faqs-tech/#faq-installer-260-q
--------------------------------------
DATABASE RESULTS
--------------------------------------
DB VIEWS:	enabled
DB PROCEDURES:	enabled
DB FUNCTIONS:	enabled
ERRORS FOUND:	0
DROPPED TABLES:	58
RENAMED TABLES:	0
QUERIES RAN:	1914

nbw_commentmeta: (0)
nbw_comments: (1)
nbw_duplicator_packages: (0)
nbw_duplicator_pro_packages: (0)
nbw_links: (0)
nbw_options: (168)
nbw_postmeta: (451)
nbw_posts: (192)
nbw_term_relationships: (17)
nbw_term_taxonomy: (6)
nbw_termmeta: (0)
nbw_terms: (6)
nbw_usermeta: (35)
nbw_users: (2)
nbw_yoast_seo_links: (96)
nbw_yoast_seo_meta: (886)
Removed '35' cache/transient rows

INSERT DATA RUNTIME: 2.6787 sec.
STEP-2 COMPLETE @ 09:57:48 - RUNTIME: 2.6982 sec.

====================================
SET SEARCH AND REPLACE LIST
====================================


********************************************************************************
DUPLICATOR PRO INSTALL-LOG
STEP-3 START @ 09:58:08
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	"utf8"
CHARSET CLIENT:	"utf8"
********************************************************************************
OPTIONS:
blogname______________: "Địa ốc Tuấn Nhàn"
postguid______________: false
fullsearch____________: false
path_old______________: "/home/eventdig/tuannhan.dvt.vn"
path_new______________: "/var/www/vhosts/congtytuannhan.com/httpdocs"
siteurl_______________: "https://congtytuannhan.com"
url_old_______________: "https://tuannhan.dvt.vn"
url_new_______________: "https://congtytuannhan.com"
maxSerializeStrlen____: 4000000
replaceMail___________: false
dbcharset_____________: "utf8"
dbcollate_____________: "utf8_general_ci"
wp_mail_______________: ""
wp_nickname___________: ""
wp_first_name_________: ""
wp_last_name__________: ""
ssl_admin_____________: false
cache_wp______________: true
cache_path____________: false
exe_safe_mode_________: false
config_mode___________: "NEW"
********************************************************************************


====================================
RUN SEARCH AND REPLACE
====================================

EVALUATE TABLE: "nbw_commentmeta"_________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "nbw_comments"____________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_duplicator_packages"_________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "nbw_duplicator_pro_packages"_____________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "nbw_links"_______________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "nbw_options"_____________________________________[ROWS:   168][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_postmeta"____________________________________[ROWS:   451][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_posts"_______________________________________[ROWS:   192][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_term_relationships"__________________________[ROWS:    17][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_term_taxonomy"_______________________________[ROWS:     6][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_termmeta"____________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "nbw_terms"_______________________________________[ROWS:     6][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_usermeta"____________________________________[ROWS:    35][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_users"_______________________________________[ROWS:     2][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_yoast_seo_links"_____________________________[ROWS:    96][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"

EVALUATE TABLE: "nbw_yoast_seo_meta"______________________________[ROWS:   886][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/eventdig/tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  2:"\/home\/eventdig\/tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  3:"%2Fhome%2Feventdig%2Ftuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  4:"\home\eventdig\tuannhan.dvt.vn" ==================> "/var/www/vhosts/congtytuannhan.com/httpdocs"
	SEARCH  5:"\\home\\eventdig\\tuannhan.dvt.vn" ===============> "\/var\/www\/vhosts\/congtytuannhan.com\/httpdocs"
	SEARCH  6:"%5Chome%5Ceventdig%5Ctuannhan.dvt.vn" ============> "%2Fvar%2Fwww%2Fvhosts%2Fcongtytuannhan.com%2Fhttpdocs"
	SEARCH  7:"//tuannhan.dvt.vn" ===============================> "//congtytuannhan.com"
	SEARCH  8:"\/\/tuannhan.dvt.vn" =============================> "\/\/congtytuannhan.com"
	SEARCH  9:"%2F%2Ftuannhan.dvt.vn" ===========================> "%2F%2Fcongtytuannhan.com"
	SEARCH 10:"//www.tuannhan.dvt.vn" ===========================> "//congtytuannhan.com"
	SEARCH 11:"\/\/www.tuannhan.dvt.vn" =========================> "\/\/congtytuannhan.com"
	SEARCH 12:"%2F%2Fwww.tuannhan.dvt.vn" =======================> "%2F%2Fcongtytuannhan.com"
	SEARCH 13:"http://congtytuannhan.com" =======================> "https://congtytuannhan.com"
	SEARCH 14:"http:\/\/congtytuannhan.com" =====================> "https:\/\/congtytuannhan.com"
	SEARCH 15:"http%3A%2F%2Fcongtytuannhan.com" =================> "https%3A%2F%2Fcongtytuannhan.com"
--------------------------------------
SCANNED:	Tables:16 	|	 Rows:1860 	|	 Cells:10316 
UPDATED:	Tables:5 	|	 Rows:274 	|	 Cells:281 
ERRORS:		0 
RUNTIME:	0.242400 sec

====================================
REMOVE LICENSE KEY
====================================

====================================
CREATE NEW ADMIN USER
====================================

====================================
CONFIGURATION FILE UPDATES
====================================
	UPDATE DB_NAME "** OBSCURED **"
	UPDATE DB_USER "** OBSCURED **"
	UPDATE DB_PASSWORD "** OBSCURED **"
	UPDATE DB_HOST ""localhost""
	UPDATE WP_CACHE true
	REMOVE WPCACHEHOME
	
*** UPDATED WP CONFIG FILE ***

====================================
HTACCESS UPDATE MODE: "NEW"
====================================
- PASS: Successfully created a new .htaccess file.
- PASS: Existing Apache '.htaccess__[HASH]' was removed

====================================
GENERAL UPDATES & CLEANUP
====================================

====================================
DEACTIVATE PLUGINS CHECK
====================================
Deactivated plugins list here: Array
(
    [0] => simple-google-recaptcha/simple-google-recaptcha.php
    [1] => js_composer/js_composer.php
)

MAINTENANCE MODE DISABLE

====================================
NOTICES TEST
====================================
No General Notices Found


====================================
CLEANUP TMP FILES
====================================

====================================
FINAL REPORT NOTICES
====================================

STEP-3 COMPLETE @ 09:58:08 - RUNTIME: 0.2897 sec. 


====================================
FINAL REPORT NOTICES LIST
====================================
-----------------------
[INFO] No general notices
	SECTIONS: general

-----------------------
[INFO] No errors in database
	SECTIONS: database

-----------------------
[INFO] No search and replace data errors
	SECTIONS: search_replace

-----------------------
[INFO] No files extraction errors
	SECTIONS: files

====================================
