<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */

add_filter( 'woocommerce_get_price', 'wpa83367_price_html', 100, 2 );
function wpa83367_price_html( $price ){
    //var_dump($price);
    $price = (int)$price;
    $money = $price/1000000000;
    if($money > 0 ){
        return "$money Tỷ";
    }
    $money2 = $price/1000000;
    return "$money2 Triệu";
}

add_action( 'wp_ajax_loadpost', 'loadpost_init' );
add_action( 'wp_ajax_nopriv_loadpost', 'loadpost_init' );
function loadpost_init() {

    ob_start(); //bắt đầu bộ nhớ đệm
    $result = $_POST['result'];
    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => 10,
        'tax_query' => array(
            array(
                    'taxonomy' => 'product_cat',
                    'field' => 'id',
                    'terms' => $result,
                    'operator' => 'AND'
            )
        )
    );

    $loop = new WP_Query( $args );
 
    if($loop->have_posts()):
        echo '<div class="products row row-small large-columns-4 medium-columns-3 small-columns-2">';
            while($loop->have_posts()):$loop->the_post();
                global $product;
                //var_dump($product);
                ?>
                    <div class="product-small col has-hover product type-product status-publish has-post-thumbnail purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"></div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-fade_in_back">
                                        <a href="<?php echo get_permalink();?>">
                                            <?php echo woocommerce_get_product_thumbnail(); ?>
                
                                        </a>
                                    </div>
                                    <div class="image-tools is-small top right show-on-hover"></div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"></div>
                                    <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a class="quick-view quick-view-added" data-prod="1138" href="#quick-view">Quick View</a>
                                    </div>
                                </div>

                                <div class="box-text box-text-products">
                                    <div class="title-wrapper">
                                        <!-- <p class="category uppercase is-smaller no-text-overflow product-cat op-7"><?php echo flatsome_woocommerce_shop_loop_category();?></p> -->
                                        <p class="name product-title woocommerce-loop-product__title">
                                            <a href="<?php echo get_permalink();?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                <?php echo get_the_title();?>
                                            </a>
                                        </p>
                                    </div>
                                    <div class="price-wrapper">
                                        <span class="price">
											<?php echo $product->get_price();?>
                                        </span>
										<?php // echo flatsome_woocommerce_shop_loop_category();?>
                                    </div>
                                    <div class="description-wrapper">
                                        <p><?php echo $product->get_short_description();?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
            endwhile;
        echo '</div>';
    endif; wp_reset_query();

    $result = ob_get_clean(); //cho hết bộ nhớ đệm vào biến $result
 
    wp_send_json_success($result); // trả về giá trị dạng json
 
    die();//bắt buộc phải có khi kết thúc
}
add_action('woocommerce_single_product_summary', 'remove_product_description_add_cart_button', 1 );
function remove_product_description_add_cart_button() { // function for deleting ...
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
}